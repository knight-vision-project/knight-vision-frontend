import React, { useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import { SocketContext } from "../SocketContext";

const DeviceStatus = () => {
  const {state} = useContext(SocketContext);
  return (
    <div>
      Device status: <span style={state != 'ERROR'? {color: 'green'}: {color: 'red'}}>{state}</span>
    </div>
  );
}

const NavBar = ({NavComp, preGlobalFilteredRows, globalFilter, setGlobalFilter}) => {
  React.useEffect(()=>{
    const paths = ['/base-config', '/warranty', '/live']
    if (paths.includes(window.location.pathname))
      document.getElementById(window.location.pathname.replace('/', '')).classList.add('active')
  }, [window.location.pathname]);

  return (
    <Navbar bg="light" expand="lg" fixed='top' style={{boxShadow: '0px 5px 5px -5px gray'}}>
      <Container fluid>
        <Navbar.Brand as={Link} to="/" title="Home" style={{alignContent: 'left'}}><img src="assets/LimeNew.png" height="24" alt='logo'/><font face = "Jura, sans-serif" size="+2" style={{verticalAlign: 'middle'}}><strong>Knight</strong>Vision</font></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-2">
            <Nav.Link  id="base-config" as={Link} to="/base-config" title="Base configuration">Configuration</Nav.Link>
          </Nav>
          <Nav className="me-2">
            <Nav.Link id="warranty" as={Link} to="/warranty"  title="Warranty configuration">Warranty</Nav.Link>
          </Nav>
          <Nav className="me-auto">
            <Nav.Link id="live" as={Link} to="/live"  title="Live Data Viewer">Live</Nav.Link>
          </Nav>
          <Nav className="ml-auto">
            <DeviceStatus/>
          </Nav>
          {NavComp?<NavComp/>: null}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;