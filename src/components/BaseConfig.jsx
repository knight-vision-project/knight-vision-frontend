import React from "react";
import { Button } from "react-bootstrap";
import { SocketContext } from '../SocketContext';

const BaseConfig = () => {
  const {socket} = React.useContext(SocketContext);

  const handleGetConfig = () => {
    console.log('button clicked')
    socket.emit('get_config', 'hi')
  }

  const handleSetConfig = () => {
    console.log('set config requested')
    socket.emit('set_config', {cellOverVoltLim: 4.20, pdDisengageTimeout: 120, cellOverVoltLimDelay: 1, idlePackPdTimeout: 120, idlePackPdTimeoutEnable: 1})
  }

  return (
    <>
      <Button onClick={handleGetConfig} >Get Config</Button>
      <Button onClick={handleSetConfig} >Set Config</Button>
    </>
  );
}

export default BaseConfig;