import React, { useContext } from "react";
import ReactTable from './Table';
import { SocketContext } from '../SocketContext';
import { useTable, useSortBy, useGroupBy, useRowSelect, useExpanded, useGlobalFilter } from 'react-table';

const StatusList = () => {
  const {jsonData, state} = useContext(SocketContext);
  
  const tableData = React.useMemo(() => {
    const keys = Object.keys(jsonData?.statusList? jsonData?.statusList: {})
    const data = keys? keys.map(item => (
      {
        name: item,
        value: jsonData.statusList[item]
      }
      )): []
    return data},
  [jsonData]);

  var tableColumns = [
    {
      Header: 'Name',
      accessor: 'name',
      visible: 1,
      Cell: ({row}) => row.values['name'],
    },
    {
      Header: 'Value',
      accessor: 'value',
      visible: 1,
      Cell: ({row}) => row.values['value']? <div typeof="button" style={{color: 'green'}}>ON</div>: <div typeof="button" style={{color: 'gray'}}>OFF</div>,
    },
  ]

  const columns = React.useMemo(() => tableColumns, []);

  const tableInstance = useTable(
    { columns: columns,
      data: tableData,
      manualPagination: true,
      manualSortBy: true,
      manualGlobalFilter: true,
      autoResetSelectedRows: true,
      autoResetPage: false,
    },
    useGlobalFilter,
    useGroupBy,
    useSortBy,
    useExpanded,
    useRowSelect,
  );

  return (
    <div style={{display: 'flex'}}>
      <div style={{flex: '1 1 auto'}}>
        <span>Hardware Errors</span>
        <ReactTable tableProps={tableInstance} loading={state.loadingData}/>
      </div>
    </div>
  );
}

export default StatusList;