import React, { useContext } from 'react';
import { SocketContext } from '../SocketContext';
import { Table } from 'react-bootstrap';

const ProtectionWarningList = () => {
  const {jsonData} = useContext(SocketContext);
  
  const tableData = React.useMemo(() => {
    const data = [
      {
        name: 'Cell Voltage',
        values: {
          value: {
            Max: jsonData['Max Cell Voltage'],
            Avg: jsonData['avg_cell_voltage'],
            Min: jsonData['Min Cell Voltage']
          },
        }
      },
      {
        name: 'Pack Voltage',
        values: {
          value: {
            Value: jsonData['Pack Voltage']
          }
        }
      },
    ]
    return data},
  [jsonData]);

  return (
    <div style={{display: 'flex'}}>
      <div style={{flex: '1 1 auto'}}>
        <Table hover size="sm">
          <thead>
            <tr>
              <th>
                Name
              </th>
              <th>
                Value
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                Cell Voltage
              </td>
              <td>
                <div>
                  <div>Max: {jsonData['Max Cell Voltage']}, Cell ID: {jsonData['cell_id_with_max_voltage']}</div>
                  <div>Avg: {jsonData['avg_cell_voltage']}</div>
                  <div>Min: {jsonData['Min Cell Voltage']}, Cell ID: {jsonData['cell_id_with_min_voltage']}</div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                Pack Voltage
              </td>
              <td>
                <div>{jsonData["Pack Voltage"]}</div>
              </td>
            </tr>
            <tr>
              <td>
                Pack Current
              </td>
              <td>
                <div>{jsonData["Pack Current"]}</div>
              </td>
            </tr>
            <tr>
              <td>
                Cell Temperature
              </td>
              <td>
                <div>Max: {jsonData["max_temperature"]}, ID: {jsonData["sensor_id_with_max_temperature"]}</div>
                <div>Min: {jsonData["min_temperature"]}, ID: {jsonData["sensor_id_with_min_temperature"]}</div>
              </td>
            </tr>
            <tr>
              <td>
                FET Temperature
              </td>
              <td>
                <div>{jsonData["temperature_of_the_Mosfets"]}</div>
              </td>
            </tr>
            <tr>
              <td>
                Cell Deviation
              </td>
              <td>
                <div>{jsonData["cell_voltage_deviation"]}</div>
              </td>
            </tr>
          </tbody>
        </Table>
      </div>
    </div>
  );
}

export default ProtectionWarningList;