import React, { useState, useEffect, useContext } from 'react';
import { SocketContext } from '../SocketContext';
import { Table } from 'react-bootstrap'

export const LiveDataViewer = () => {
  const [response, setResponse] = useState(null);

  const {socket, mydata: data, jsonData, connected, error, state} = useContext(SocketContext);

  const formatTimeStamp = (timestamp) => {
    const date = new Date(timestamp);
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
    const miliseconds = String(date.getMilliseconds()).padStart(3, '0');
    return `${day}-${month}-${date.getFullYear()}, ${hours}:${minutes}:${seconds}.${miliseconds}`;
  }

  return (
    <div>
      <h4 style={{textAlign: 'center'}}>Live Data</h4>
      <ul>
        {
          state != 'ERROR'?
          (<li>State: <span style={{color: 'green'}}>{state}</span></li>):
          (
            <>
              <li>State: <span style={{color: 'red'}}>{state}</span></li>
              <li>Error: {error}</li>
            </>
          )
        }
      </ul>

      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>Time</th>
            <th>ID</th>
            <th>Data</th>
            <th>Parameters</th>
          </tr>
        </thead>
        <tbody>
          {data? 
            Object.keys(data).map(item => (
              <tr key={data[item].id}>
                <td>{formatTimeStamp(data[item].time*1000)}</td>
                <td>{data[item].id}</td>
                <td>{data[item].data.match(/.{1,2}/g).join(' ')}</td>
                <td><pre>{JSON.stringify(data[item].json_data, null, 2)}</pre></td>
              </tr>
            )): null
          }
        </tbody>
      </Table>
    </div>
  );
};
