import React, { createContext } from 'react';

export const SocketContext = createContext();
export const DataContext = createContext();