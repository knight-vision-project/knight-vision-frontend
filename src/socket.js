import socketIOClient from 'socket.io-client';


let socket;

export const initSocket = () => {
  if (!socket) {
    socket = socketIOClient('http://localhost:56789');
  }
};

export const getSocket = () => {
  if (!socket) {
    throw new Error('Socket.io client has not been initialized.');
  }
  return socket;
};

export default socket;