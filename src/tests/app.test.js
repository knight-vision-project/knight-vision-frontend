
import React from 'react'
import { render, event, fireEvent, screen } from '@testing-library/react'

import TestApp from '../components/Component'
test('button click toggles theme', () => {
  // Render the component
  render(<TestApp />);

  // Get the button element
  const button = screen.getByText('Current theme: light');

  // // Simulate a button click
  fireEvent.click(button);

  // // Check if the theme has changed to 'dark'
  expect(screen.getByText('Current theme: dark')).toBeInTheDocument();
});

import App from '../App'
test('homepage loading', () => {
  render(<App />);
  expect(screen.getByText('Learn React')).toBeInTheDocument();
});